package pl.put.poznan.krzeminski_marcin.check_moon


import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.ceil
import kotlin.math.floor


class MoonPhaseCalculator {


    fun findLastNewMoon(methodName: String, now: Calendar): Calendar {
        var lastMoon = now.clone() as Calendar
        var result = 1
        lastMoon.add(Calendar.DAY_OF_MONTH, 1)
        while (result !== 0) {
            lastMoon.add(Calendar.DAY_OF_MONTH, -1)
            result = calc(methodName, lastMoon.get(Calendar.YEAR), lastMoon.get(Calendar.MONTH), lastMoon.get(Calendar.DAY_OF_MONTH))
        }
        return lastMoon
    }

    fun findNextFullMoon(methodName: String, now: Calendar): Calendar {
        var nextMoon = now.clone() as Calendar
        var result = 1
        nextMoon.add(Calendar.DAY_OF_MONTH, -1)
        while (result !== 15) {
            nextMoon.add(Calendar.DAY_OF_MONTH, 1)
            result = calc(methodName, nextMoon.get(Calendar.YEAR), nextMoon.get(Calendar.MONTH), nextMoon.get(Calendar.DAY_OF_MONTH))
        }
        return nextMoon
    }



    fun findPhasesInYear(methodName: String, year: Int): ArrayList<Calendar> {
        var startDate = Calendar.getInstance()
        var endDate = Calendar.getInstance()
        var fullMoonsList: ArrayList<Calendar> = ArrayList()


        startDate.set(year, 0, 1)
        endDate.set(year, 11, 31)
        var result: Int
        var tempDate: Calendar
        while (endDate.compareTo(startDate) >= 0) {

            result = calc(
                methodName,
                startDate.get(Calendar.YEAR),
                startDate.get(Calendar.MONTH) + 1,
                startDate.get(Calendar.DAY_OF_MONTH)
            )

            if (result == 15) {
                tempDate = startDate.clone() as Calendar
                fullMoonsList.add(tempDate)
            }
            startDate.add(Calendar.DAY_OF_MONTH, 1)
        }

        return fullMoonsList
    }

    fun calc(methodName: String, year: Int, month: Int, day: Int): Int {

        var res: Int
        if (methodName.equals("simple")) res = simple(year, month, day)
        else if (methodName.equals("conway")) res = conway(year, month, day)
        else if (methodName.equals("trig1")) res = trig1(year, month, day)
        else if (methodName.equals("trig2")) res = trig2(year, month, day)
        else res = 1
        return res
    }

    fun simple(year: Int, month: Int, day: Int): Int {
        var lp = 2551443;
        var now = Date(year, month - 1, day, 13, 52, 0)
        var new_moon = Date(1900, 0, 1, 13, 52, 0)
        var phase = ((now.getTime() - new_moon.getTime()) / 1000) % lp
        var a =  floor((phase / (24 * 3600).toDouble())).toInt()  +1
        if(a==30){
            a=0
        }
        return a
    }

    fun conway(year: Int, month: Int, day: Int): Int {
        var r = (year % 100).toDouble()
        r %= 19
        if (r > 9) {
            r -= 19
        }
        r = ((r * 11) % 30) + month.toDouble() + day.toDouble()
        if (month < 3) {
            r += 2
        }
        if (year < 2000) r = r - 4 else r = r - 8.3
        var w = (Math.floor(r + 0.5) % 30).toInt()
        if (w < 0) return (w + 30) else return w
    }


    fun trig1(year: Int, month: Int, day: Int): Int {
        var thisJD = julday(year, month, day);
        var degToRad = 3.14159265 / 180;

        var K0 = Math.floor((year - 1900) * 12.3685);
        var T = (year - 1899.5) / 100;
        var T2 = T * T;
        var T3 = T * T * T;
        var J0 = 2415020 + 29 * K0;
        var F0 = 0.0001178 * T2 - 0.000000155 * T3 + (0.75933 + 0.53058868 * K0) - (0.000837 * T + 0.000335 * T2);
        var M0 = 360 * (GetFrac(K0 * 0.08084821133)) + 359.2242 - 0.0000333 * T2 - 0.00000347 * T3;
        var M1 = 360 * (GetFrac(K0 * 0.07171366128)) + 306.0253 + 0.0107306 * T2 + 0.00001236 * T3;
        var B1 = 360 * (GetFrac(K0 * 0.08519585128)) + 21.2964 - (0.0016528 * T2) - (0.00000239 * T3);
        var oldJ = 0
        var phase = 0
        var jday = 0
        while (jday < thisJD) {
            var F = F0 + 1.530588 * phase;
            var M5 = (M0 + phase * 29.10535608) * degToRad;
            var M6 = (M1 + phase * 385.81691806) * degToRad;
            var B6 = (B1 + phase * 390.67050646) * degToRad;
            F -= 0.4068 * Math.sin(M6) + (0.1734 - 0.000393 * T) * Math.sin(M5);
            F += 0.0161 * Math.sin(2 * M6) + 0.0104 * Math.sin(2 * B6);
            F -= 0.0074 * Math.sin(M5 - M6) - 0.0051 * Math.sin(M5 + M6);
            F += 0.0021 * Math.sin(2 * M5) + 0.0010 * Math.sin(2 * B6 - M6);
            F += 0.5 / 1440;
            oldJ = jday;
            jday = J0.toInt() + 28 * phase + floor(F).toInt()
            phase++;
        }
        return (thisJD - oldJ) % 30;
    }


    fun trig2(year: Int, month: Int, day: Int): Int {
        var n = Math.floor(12.37 * (year - 1900 + ((1.0 * month - 0.5) / 12.0)))
        var RAD = 3.14159265 / 180.0
        var t = n / 1236.85
        var t2 = t * t
        var asa = 359.2242 + 29.105356 * n
        var am = 306.0253 + 385.816918 * n + 0.010730 * t2;
        var xtra = 0.75933 + 1.53058868 * n + ((1.178e-4) - (1.55e-7) * t) * t2;
        xtra += (0.1734 - 3.93e-4 * t) * Math.sin(RAD * asa) - 0.4068 * Math.sin(RAD * am);
        var i: Int
        if (xtra > 0.0) {
            i = floor(xtra).toInt()
        } else i = ceil(xtra - 1.0).toInt()
        var j1 = julday(year, month, day);
        var jd = (2415020 + 28 * n).toInt() + i
        return (j1 - jd + 30).toInt() % 30
    }


    fun GetFrac(fr: Double): Double {
        return (fr - Math.floor(fr));
    }


    fun julday(year: Int, month: Int, day: Int): Int {
//        if (year < 0) { year++; }
        var jy = year
        var jm = month + 1;
        if (month <= 2) {
            jy--; jm += 12; }
        var jul = Math.floor(365.25 * jy) + Math.floor(30.6001 * jm) + day + 1720995;
        if (day + 31 * (month + 12 * year) >= (15 + 31 * (10 + 12 * 1582))) {
            var ja = Math.floor(0.01 * jy);
            jul = jul + 2 - ja + Math.floor(0.25 * ja);
        }
        return jul.toInt();
    }
}