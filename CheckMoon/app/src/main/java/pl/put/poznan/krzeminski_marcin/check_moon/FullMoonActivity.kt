package pl.put.poznan.krzeminski_marcin.check_moon

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_full_moon.*
import java.text.SimpleDateFormat
import java.util.*


class FullMoonActivity : AppCompatActivity() {

    var year = Calendar.getInstance().get(Calendar.YEAR)
    var calculator = MoonPhaseCalculator()
    var methodName = "simple"
    var format1 = SimpleDateFormat("dd-MM-yyyy")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_moon)
        inputYear.setText(year.toString())
        textView13.setText("Księżyc będzie w pełni:")
        findAndShowFullMoons()

        val extras = intent.extras ?: return
        methodName = extras.getString("methodName")
        addYearButton.setOnClickListener {
            year += 1
            inputYear.setText(year.toString())
//            findAndShowFullMoons()
        }
        minusYearButton.setOnClickListener {
            year -= 1
            inputYear.setText(year.toString())
//            findAndShowFullMoons()
        }

        inputYear.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

                if (s.length != 0) {
                    if (s.length <= 4) {
                        var tempYear = s.toString().toInt()
                        year = tempYear
                        if (tempYear >= 1900 && tempYear <= 2200) {
                            var c = textView13 as TextView
                            c.setText("Księżyc będzie w pełni:")

                            findAndShowFullMoons()
                        }
                        else{
                            var c = textView13 as TextView
                            c.setText("Podaj rok z zakresu 1900 - 2200")
                            var i = 0
                            while(i<12){
                                var b = linearLayout.getChildAt(i) as TextView
                                b.setText("")
                                i++
                            }
                        }

                    }
                    else{
                        if(year>=9999){
                            year=9999
                            var i = 0
                            var c = textView13 as TextView
                            c.setText("Podaj rok z zakresu 1900 - 2200")
                            while(i<12){
                                var b = linearLayout.getChildAt(i) as TextView
                                b.setText("")
                                i++
                            }
                        }
                        inputYear.setText(year.toString())}
                }
            }
        })
    }


    fun findAndShowFullMoons() {
        var fullMoonsList = calculator.findPhasesInYear(methodName, year)
        var i = 0
        for (a in fullMoonsList) {
            var b = linearLayout.getChildAt(i) as TextView
            b.setText(format1.format(a.getTime()))
            i++
            if (i == 12) break
        }
    }


}
