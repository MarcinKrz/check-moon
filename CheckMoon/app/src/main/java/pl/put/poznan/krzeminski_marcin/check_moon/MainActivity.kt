package pl.put.poznan.krzeminski_marcin.check_moon

import android.app.Activity
import android.content.Intent

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {

    var calculator = MoonPhaseCalculator()
    val calendar = Calendar.getInstance()
    var format1 = SimpleDateFormat("dd-MM-yyyy")
    val REQUEST_CODE = 200
    var methodName = "trig1"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        calculateData()
    }


    fun calculateData() {
        var res = calculator.calc(
            methodName,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH) + 1,
            calendar.get(Calendar.DAY_OF_MONTH)
        )

        val percent: Int
        if (res > 15) {
            percent = (((30 - res.toDouble()) / 15) * 100).toInt()
        } else {
            percent = ((res.toDouble() / 15) * 100).toInt()
        }

        if (percent <= 50) {
            imageMoon.setImageResource(R.drawable.neew)
        } else {
            imageMoon.setImageResource(R.drawable.fuull)
        }

        moonTodayText.text = "Dzisiaj: " + percent.toString() + "%"
        var lastNewMoon = calculator.findLastNewMoon(methodName, calendar)
        val formatted = format1.format(lastNewMoon.getTime())
        lastNewMoonText.text = "Ostatni nów: ${formatted} r."

        val nextFullMoon = calculator.findNextFullMoon(methodName, calendar)
        val formatted2 = format1.format(nextFullMoon.getTime())
        nextFullMoonText.text = "Następna pełnia: ${formatted2} r."
    }


    fun goToFullMoonActivity(view: View) {
        val intent = Intent(this, FullMoonActivity::class.java)
            .putExtra("methodName", methodName)
        startActivity(intent)
    }

    fun goToSettings(view: View) {
        val intent = Intent(this, AlgorithmsListActivity::class.java)
            .putExtra("methodName", methodName)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if ((requestCode == REQUEST_CODE) && (resultCode == Activity.RESULT_OK)) {
            if (data != null) {
                if (data.hasExtra("methodName"))
                    methodName = data.extras.getString("methodName")
                calculateData()
            }
        }
    }

}
