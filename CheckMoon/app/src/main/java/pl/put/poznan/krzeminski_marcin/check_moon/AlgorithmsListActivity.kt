package pl.put.poznan.krzeminski_marcin.check_moon

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_algorithms_list.*

class AlgorithmsListActivity : AppCompatActivity() {

    var METHOD_NAME="trig1"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_algorithms_list)

        val extras = intent.extras?:return
        METHOD_NAME = extras.getString("methodName")
        when(METHOD_NAME){
            "simple" -> chosenAlgorithms.check(R.id.simpleButton )
            "conway" -> chosenAlgorithms.check(R.id.conwayButton )
            "trig1" -> chosenAlgorithms.check(R.id.trig1Button )
            "trig2" -> chosenAlgorithms.check(R.id.trig2Button )
        }

        chosenAlgorithms.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.simpleButton -> METHOD_NAME = "simple"
                R.id.conwayButton -> METHOD_NAME = "conway"
                R.id.trig1Button -> METHOD_NAME = "trig1"
                R.id.trig2Button -> METHOD_NAME = "trig2"
            }
            println(METHOD_NAME)
        }
    }

    override fun finish() {
        val data = Intent()
        data.putExtra("methodName",METHOD_NAME)
        setResult(Activity.RESULT_OK,data)
        super.finish()
    }
}
